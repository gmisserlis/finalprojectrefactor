/**
 * Creates Fields of information for the Edge Convert MySQL Database
 */
import java.util.StringTokenizer;

public class EdgeField {
   private int numFigure, tableID, tableBound, fieldBound, dataType, varcharValue;
   private String name, defaultValue;
   private boolean disallowNull, isPrimaryKey;
   private static String[] strDataType = {"Varchar", "Boolean", "Integer", "Double"};
   public static final int VARCHAR_DEFAULT_LENGTH = 1;
   
   public EdgeField(String inputString) {
      StringTokenizer st = new StringTokenizer(inputString, EdgeConvertFileParser.DELIM);
      numFigure = Integer.parseInt(st.nextToken());
      name = st.nextToken();
      tableID = 0;
      tableBound = 0;
      fieldBound = 0;
      disallowNull = false;
      isPrimaryKey = false;
      defaultValue = "";
      varcharValue = VARCHAR_DEFAULT_LENGTH;
      dataType = 0;
   }
   
   /************* accessors *************/
   public int getNumFigure() {
      return numFigure;
   }
   
   public String getName() {
      return name;
   }
   
   public int getTableID() {
      return tableID;
   }
   
   public int getTableBound() {
      return tableBound;
   }  

   public int getFieldBound() {
      return fieldBound;
   }

   public boolean getDisallowNull() {
      return disallowNull;
   }
      
   public boolean getIsPrimaryKey() {
      return isPrimaryKey;
   }
      
   public String getDefaultValue() {
      return defaultValue;
   }
      
   public int getVarcharValue() {
      return varcharValue;
   }
   
   public int getDataType() {
      return dataType;
   }
      
   public static String[] getStrDataType() {
      return strDataType;
   }
   /************* accessors END *************/
   
   
   /************* mutators *************/
   public void setTableID(int value) {
      tableID = value;
   }
   
   public void setTableBound(int value) {
      tableBound = value;
   }
   
   public void setFieldBound(int value) {
      fieldBound = value;
   }
   
   public void setDisallowNull(boolean value) {
      disallowNull = value;
   }

   public void setIsPrimaryKey(boolean value) {
      isPrimaryKey = value;
   }

   public void setDefaultValue(String value) {
      defaultValue = value;
   }

   public void setVarcharValue(int value) {
      if (value > 0) {
         varcharValue = value;
      }
   }
   
   public void setDataType(int value) {
      if (value >= 0 && value < strDataType.length) {
         dataType = value;
      }
   }
   /************* mutators END ***************/   
   
   public String toString() {
      return numFigure + EdgeConvertFileParser.DELIM +
      name + EdgeConvertFileParser.DELIM +
      tableID + EdgeConvertFileParser.DELIM +
      tableBound + EdgeConvertFileParser.DELIM +
      fieldBound + EdgeConvertFileParser.DELIM +
      dataType + EdgeConvertFileParser.DELIM +
      varcharValue + EdgeConvertFileParser.DELIM +
      isPrimaryKey + EdgeConvertFileParser.DELIM +
      disallowNull + EdgeConvertFileParser.DELIM +
      defaultValue;
   }
}
